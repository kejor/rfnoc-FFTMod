
//
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

//
module noc_block_efft #(
  parameter NOC_ID = 64'h9CA1EDACA5FE2866,
  parameter STR_SINK_FIFOSIZE = 11)
(
  input bus_clk, input bus_rst,
  input ce_clk, input ce_rst,
  input  [63:0] i_tdata, input  i_tlast, input  i_tvalid, output i_tready,
  output [63:0] o_tdata, output o_tlast, output o_tvalid, input  o_tready,
  output [63:0] debug
);

  ////////////////////////////////////////////////////////////
  //
  // RFNoC Shell
  //
  ////////////////////////////////////////////////////////////
  wire [31:0] set_data;
  wire [7:0]  set_addr;
  wire        set_stb;
  reg  [63:0] rb_data;
  wire [7:0]  rb_addr;

  wire [63:0] cmdout_tdata, ackin_tdata;
  wire        cmdout_tlast, cmdout_tvalid, cmdout_tready, ackin_tlast, ackin_tvalid, ackin_tready;

  wire [63:0] str_sink_tdata, str_src_tdata;
  wire        str_sink_tlast, str_sink_tvalid, str_sink_tready, str_src_tlast, str_src_tvalid, str_src_tready;

  wire [15:0] src_sid;
  wire [15:0] next_dst_sid, resp_out_dst_sid;
  wire [15:0] resp_in_dst_sid;

  wire        clear_tx_seqnum;

  noc_shell #(
    .NOC_ID(NOC_ID),
    .STR_SINK_FIFOSIZE(STR_SINK_FIFOSIZE))
  noc_shell (
    .bus_clk(bus_clk), .bus_rst(bus_rst),
    .i_tdata(i_tdata), .i_tlast(i_tlast), .i_tvalid(i_tvalid), .i_tready(i_tready),
    .o_tdata(o_tdata), .o_tlast(o_tlast), .o_tvalid(o_tvalid), .o_tready(o_tready),
    // Computer Engine Clock Domain
    .clk(ce_clk), .reset(ce_rst),
    // Control Sink
    .set_data(set_data), .set_addr(set_addr), .set_stb(set_stb),
    .rb_stb(1'b1), .rb_data(rb_data), .rb_addr(rb_addr),
    // Control Source
    .cmdout_tdata(cmdout_tdata), .cmdout_tlast(cmdout_tlast), .cmdout_tvalid(cmdout_tvalid), .cmdout_tready(cmdout_tready),
    .ackin_tdata(ackin_tdata), .ackin_tlast(ackin_tlast), .ackin_tvalid(ackin_tvalid), .ackin_tready(ackin_tready),
    // Stream Sink
    .str_sink_tdata(str_sink_tdata), .str_sink_tlast(str_sink_tlast), .str_sink_tvalid(str_sink_tvalid), .str_sink_tready(str_sink_tready),
    // Stream Source
    .str_src_tdata(str_src_tdata), .str_src_tlast(str_src_tlast), .str_src_tvalid(str_src_tvalid), .str_src_tready(str_src_tready),
    // Stream IDs set by host
    .src_sid(src_sid),                   // SID of this block
    .next_dst_sid(next_dst_sid),         // Next destination SID
    .resp_in_dst_sid(resp_in_dst_sid),   // Response destination SID for input stream responses / errors
    .resp_out_dst_sid(resp_out_dst_sid), // Response destination SID for output stream responses / errors
    // Misc
    .vita_time('d0), .clear_tx_seqnum(clear_tx_seqnum),
    .debug(debug));

  ////////////////////////////////////////////////////////////
  //
  // AXI Wrapper
  // Convert RFNoC Shell interface into AXI stream interface
  //
  ////////////////////////////////////////////////////////////
  wire [31:0] m_axis_data_tdata;
  wire        m_axis_data_tlast;
  wire        m_axis_data_tvalid;
  wire        m_axis_data_tready;

  wire [31:0] s_axis_data_tdata;
  wire        s_axis_data_tlast;
  wire        s_axis_data_tvalid;
  wire        s_axis_data_tready;

  wire [31:0] m_axis_config_tdata;
  wire        m_axis_config_tlast;
  wire        m_axis_config_tvalid;
  wire        m_axis_config_tready;

  localparam [7:0] SR_AXI_CONFIG_BASE = 128;
  localparam [7:0] NUM_AXI_CONFIG_BUS = 1;

  axi_wrapper #(
    .SIMPLE_MODE(1),
    .SR_AXI_CONFIG_BASE(SR_AXI_CONFIG_BASE),
    .NUM_AXI_CONFIG_BUS(NUM_AXI_CONFIG_BUS))
  axi_wrapper (
    .clk(ce_clk), .reset(ce_rst),
    .clear_tx_seqnum(clear_tx_seqnum),
    .next_dst(next_dst_sid),
    .set_stb(set_stb), .set_addr(set_addr), .set_data(set_data),
    .i_tdata(str_sink_tdata), .i_tlast(str_sink_tlast), .i_tvalid(str_sink_tvalid), .i_tready(str_sink_tready),
    .o_tdata(str_src_tdata), .o_tlast(str_src_tlast), .o_tvalid(str_src_tvalid), .o_tready(str_src_tready),
    .m_axis_data_tdata(m_axis_data_tdata),
    .m_axis_data_tlast(m_axis_data_tlast),
    .m_axis_data_tvalid(m_axis_data_tvalid),
    .m_axis_data_tready(m_axis_data_tready),
    .m_axis_data_tuser(),
    .s_axis_data_tdata(s_axis_data_tdata),
    .s_axis_data_tlast(s_axis_data_tlast),
    .s_axis_data_tvalid(s_axis_data_tvalid),
    .s_axis_data_tready(s_axis_data_tready),
    .s_axis_data_tuser(),
    .m_axis_config_tdata(m_axis_config_tdata),
    .m_axis_config_tlast(m_axis_config_tlast),
    .m_axis_config_tvalid(m_axis_config_tvalid),
    .m_axis_config_tready(m_axis_config_tready),
    .m_axis_pkt_len_tdata(),
    .m_axis_pkt_len_tvalid(),
    .m_axis_pkt_len_tready());

  ////////////////////////////////////////////////////////////
  //
  // User code
  //
  ////////////////////////////////////////////////////////////
  // NoC Shell registers 0 - 127,
  // User register address space starts at 128

  // Control Source Unused
  assign cmdout_tdata  = 64'd0;
  assign cmdout_tlast  = 1'b0;
  assign cmdout_tvalid = 1'b0;
  assign ackin_tready  = 1'b1;

  localparam [7:0] SR_USER_REG_BASE = 130;
  localparam [7:0] SR_FFT_RESET = SR_USER_REG_BASE;

  wire fft_rst;
  setting_reg #(
    .my_addr(SR_FFT_RESET), .awidth(8), .width(1))
  sr_fft_reset (
    .clk(ce_clk), .rst(ce_rst),
    .strobe(set_stb), .addr(set_addr), .in(set_data), .out(fft_rst), .changed());

  // rb_stb set to 1'b1 on NoC Shell
  always @(posedge ce_clk) begin
    case(rb_addr)
      8'd0 : rb_data <= {63'd0, fft_rst};
      default : rb_data <= 64'h0BADC0DE0BADC0DE;
    endcase
  end

  wire [31:0] fft_data_tdata;
  wire [15:0] fft_data_tuser;
  wire fft_data_tvalid, fft_data_tlast, fft_data_tready;

  efft efft (
    .aclk(ce_clk),                                                // input wire aclk
    .aresetn(~(ce_rst | fft_rst)),                                          // input wire aresetn
    .s_axis_config_tdata(m_axis_config_tdata[23:0]),                  // input wire [23 : 0] s_axis_config_tdata
    .s_axis_config_tvalid(m_axis_config_tvalid),                // input wire s_axis_config_tvalid
    .s_axis_config_tready(m_axis_config_tready),                // output wire s_axis_config_tready
    .s_axis_data_tdata(m_axis_data_tdata),                      // input wire [31 : 0] s_axis_data_tdata
    .s_axis_data_tvalid(m_axis_data_tvalid),                    // input wire s_axis_data_tvalid
    .s_axis_data_tready(m_axis_data_tready),                    // output wire s_axis_data_tready
    .s_axis_data_tlast(m_axis_data_tlast),                      // input wire s_axis_data_tlast
    .m_axis_data_tdata(fft_data_tdata),                      // output wire [31 : 0] m_axis_data_tdata
    .m_axis_data_tuser(fft_data_tuser),                      // output wire [15 : 0] m_axis_data_tuser
    .m_axis_data_tvalid(fft_data_tvalid),                    // output wire m_axis_data_tvalid
    .m_axis_data_tready(fft_data_tready),                    // input wire m_axis_data_tready
    .m_axis_data_tlast(fft_data_tlast),                      // output wire m_axis_data_tlast
    .event_frame_started(),                  // output wire event_frame_started
    .event_tlast_unexpected(),            // output wire event_tlast_unexpected
    .event_tlast_missing(),                  // output wire event_tlast_missing
    .event_status_channel_halt(),      // output wire event_status_channel_halt
    .event_data_in_channel_halt(),    // output wire event_data_in_channel_halt
    .event_data_out_channel_halt()  // output wire event_data_out_channel_halt
  );

  generate

  efft_shift #(
    .MAX_FFT_SIZE_LOG2(12),
    .WIDTH(32))
  efft_shift (
    .clk(ce_clk),
    .reset(ce_rst | fft_rst),

    .fft_size_log2_tvalid(m_axis_config_tvalid),
    .fft_size_log2_tready(m_axis_config_tready),
    .fft_size_log2_tdata(m_axis_config_tdata[3:0]),

    .fft_data_tvalid(fft_data_tvalid),
    .fft_data_tready(fft_data_tready),
    .fft_data_tlast(fft_data_tlast),
    .fft_data_tuser(fft_data_tuser[11:0]),
    .fft_data_tdata(fft_data_tdata),

    .fft_shifted_data_tvalid(s_axis_data_tvalid),
    .fft_shifted_data_tready(s_axis_data_tready),
    .fft_shifted_data_tlast(s_axis_data_tlast),
    .fft_shifted_data_tdata(s_axis_data_tdata)
  );

  endgenerate

endmodule
