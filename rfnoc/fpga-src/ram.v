module ram #(
  parameter DWIDTH = 32,
  parameter AWIDTH = 12)
  (
  input clk,
  input read_en,
  input write_en,
  input [AWIDTH-1:0] addr,
  input [DWIDTH-1:0] data_in,
  output reg [DWIDTH-1:0] data_out = 'd0
  );
  
  reg [DWIDTH-1:0] ram [(1<<AWIDTH)-1:0];
  
  always @(posedge clk) begin
    if(write_en) begin
        ram[addr] <= data_in;
    end
    if(read_en) begin
      data_out <= ram[addr];
    end
  end
  
endmodule
