module efft_shift #(
  parameter MAX_FFT_SIZE_LOG2 = 12,
  parameter WIDTH = 32)
  (
  input clk,
  input reset,

  input fft_size_log2_tvalid,
  output fft_size_log2_tready,
  input [$clog2(MAX_FFT_SIZE_LOG2)-1:0] fft_size_log2_tdata,

  input fft_data_tvalid,
  output fft_data_tready,
  input fft_data_tlast,
  input [MAX_FFT_SIZE_LOG2-1:0] fft_data_tuser,
  input [WIDTH-1:0] fft_data_tdata,

  output fft_shifted_data_tvalid,
  input fft_shifted_data_tready,
  output fft_shifted_data_tlast,
  output [WIDTH-1:0] fft_shifted_data_tdata
  );

  reg [MAX_FFT_SIZE_LOG2-1:0] fft_size;
  reg [$clog2(MAX_FFT_SIZE_LOG2)-1:0] fft_size_log2_latch;
  reg which_ram;
  wire ram0_write_enable;
  wire ram1_write_enable;
  wire ram0_read_enable;
  wire ram1_read_enable;
  wire [WIDTH-1:0] ram0_data_out;
  wire [WIDTH-1:0] ram1_data_out;
  reg [MAX_FFT_SIZE_LOG2-1:0] addr0;
  reg [MAX_FFT_SIZE_LOG2-1:0] addr1;
  //wire load_and_download = ((addr0 < fft_size/2) & (addr0 != 0))| ((addr1 < fft_size/2) & (addr0 != 0));
  wire load_and_download = (addr0 < fft_size/2) | (addr1 < fft_size/2);
  reg download_finished;
  reg load_finished;
  reg [WIDTH-1:0] fft_data_tdata_latch;
  reg fft_data_tvalid_latch;
  reg fft_shifted_data_tready_latch;

  ram #(
    .DWIDTH(WIDTH),
    .AWIDTH(MAX_FFT_SIZE_LOG2))
  ram0(
    .clk(clk),
    .read_en(ram0_read_enable),
    .write_en(ram0_write_enable),
    .addr(addr0),
    .data_in(fft_data_tdata),
    .data_out(ram0_data_out)
    );

  ram #(
    .DWIDTH(WIDTH),
    .AWIDTH(MAX_FFT_SIZE_LOG2))
  ram1(
    .clk(clk),
    .read_en(ram1_read_enable),
    .write_en(ram1_write_enable),
    .addr(addr1),
    .data_in(fft_data_tdata),
    .data_out(ram1_data_out)
    );

  assign fft_shifted_data_tdata = (((!load_and_download) | ((addr0 == 0) & which_ram) | ((addr1 == 0) & !which_ram)) & !fft_shifted_data_tlast) ? fft_data_tdata_latch : which_ram ? ram0_data_out : ram1_data_out;
  assign fft_shifted_data_tvalid = !load_and_download ? fft_data_tvalid_latch : (!download_finished) ? 1 : 0;
  assign fft_shifted_data_tlast = (download_finished) ? 1 : 0;
  assign fft_data_tready = !load_and_download ? (!fft_shifted_data_tvalid | fft_shifted_data_tready) : (fft_data_tvalid & !load_finished) ? 1 : 0;
  assign fft_size_log2_tready = reset ? 0 : fft_size_log2_tvalid;
  assign ram0_write_enable = (!which_ram & (addr0 < fft_size/2)) ? 1 : 0;
  assign ram1_write_enable = (which_ram & (addr1 < fft_size/2)) ? 1 : 0;
  assign ram0_read_enable = which_ram & !download_finished & (addr1 < fft_size/2);
  assign ram1_read_enable = !which_ram & !download_finished & (addr0 < fft_size/2);


  always @(posedge clk) begin
    if(reset) begin
      fft_size <= 0;
      which_ram <= 0;
      load_finished <= 0;
      download_finished <= 0;
      addr0 <= 0;
      addr1 <= 0;
      fft_size_log2_latch <= 0;
      fft_data_tdata_latch <= 0;
      fft_data_tvalid_latch <= 0;
      fft_shifted_data_tready_latch <= 0;
    end
    else begin
      if(fft_size_log2_tready & fft_size_log2_tvalid) begin
         fft_size_log2_latch <= fft_size_log2_tdata;
      end
      
      fft_data_tdata_latch <= fft_data_tdata;
      //fft_data_tvalid_latch <= ((fft_data_tready & fft_data_tvalid) | (fft_data_tvalid_latch & !fft_shifted_data_tready));
      fft_data_tvalid_latch <= fft_data_tvalid;
      fft_shifted_data_tready_latch <= fft_shifted_data_tready;

      fft_size <= 1 << fft_size_log2_latch;

      if(fft_data_tlast) begin
        which_ram <= ~which_ram;
        addr0 <= 0;
        addr1 <= 0;
      end

      if(which_ram) begin
        if(load_and_download) begin
          if(fft_data_tvalid & fft_data_tready) begin
            if(addr1 == (fft_size/2-1)) begin
              addr1 <= addr1 + 1;
              load_finished <= 1;
            end
            else begin
              addr1 <= addr1 + 1;
            end
          end

          if(fft_shifted_data_tvalid & fft_shifted_data_tready) begin
            if(addr0 == (fft_size/2)-1) begin
              addr0 <= addr0 +1;
              download_finished <= 1;
            end
            else begin
              addr0 <= addr0 + 1;
            end
          end
        end
        else begin
          load_finished <= 0;
          download_finished <= 0;
        end
      end
      else begin
        if(load_and_download) begin
          if(fft_data_tvalid & fft_data_tready) begin
            if(addr0 == (fft_size/2-1)) begin
              addr0 <= addr0 + 1;
              load_finished <= 1;
            end
            else begin
              addr0 <= addr0 + 1;
            end
          end

          if(fft_shifted_data_tvalid & fft_shifted_data_tready) begin
            if(addr1 == (fft_size/2)-1) begin
              addr1 <= addr1 + 1;
              download_finished <= 1;
            end
            else begin
              addr1 <= addr1 + 1;
            end 
          end
        end
        else begin
          load_finished <= 0;
          download_finished <= 0;
        end
      end
    end
  end

endmodule
