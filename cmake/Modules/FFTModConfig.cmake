INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_FFTMOD FFTMod)

FIND_PATH(
    FFTMOD_INCLUDE_DIRS
    NAMES FFTMod/api.h
    HINTS $ENV{FFTMOD_DIR}/include
        ${PC_FFTMOD_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    FFTMOD_LIBRARIES
    NAMES gnuradio-FFTMod
    HINTS $ENV{FFTMOD_DIR}/lib
        ${PC_FFTMOD_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(FFTMOD DEFAULT_MSG FFTMOD_LIBRARIES FFTMOD_INCLUDE_DIRS)
MARK_AS_ADVANCED(FFTMOD_LIBRARIES FFTMOD_INCLUDE_DIRS)

